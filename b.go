package b

import (
	"fmt"

	"bitbucket.org/bigwhite/f"
)

func CallB() {
	fmt.Println("call B: master branch")
	fmt.Println("   --> call F:")
	fmt.Printf("\t")
	f.CallF()
	fmt.Println("   --> call F end")
}
